#!/opt/local/bin/python

from random import randrange

def quick_sort(array):
    '''
    Sorts only numbers.
    '''
    if array == []:
        return []
    pivot = array.pop(randrange(len(array)))
    left = quick_sort([x for x in array if x < pivot])
    right = quick_sort([x for x in array if x >= pivot])
    return left + [pivot] + right

if __name__ == '__main__':
    a = [1,22,3,4,.3]
    print(quick_sort(a))
