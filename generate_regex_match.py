#!/usr/bin/env python3
#encoding: utf-8

'''
Credit: nooodl@freenode
'''
import re
import itertools

s = 'abc[de][fg]'

options = re.findall(r'\[(..)\]', s)
format = re.sub(r'\[..\]', '%s', s)
print([format % x for x in itertools.product(*options)])
