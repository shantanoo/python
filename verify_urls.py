#!/usr/bin/env python3.1

import sys

import re
import os
from http import client
from urllib.parse import urlparse

class url(object):
    def __init__(self):
        self.urls = {}
        self.p = re.compile(r'(ftp|http|https)://([0-9a-zA-Z &=\-%\.\/])+')
    
    def default_port(self, scheme):
        p = {}
        p['http'] = '80'
        p['https'] = '443'
        p['ftp'] = '21'
        try:
            return p[scheme]
        except:
            return None
    
    def parse_line(self, line):
        i = self.p.finditer(line)
        for m in i:
            o = urlparse(m.group())
            port = o.port
            scheme = o.scheme
            hostname = o.hostname
            path = o.path
            if scheme not in self.urls:
                self.urls[scheme] = dict()
            if hostname not in self.urls[scheme]:
                self.urls[scheme][hostname] = {}
            if not port:
                port = self.default_port(scheme)
            if not port in self.urls[scheme][hostname]:
                self.urls[scheme][hostname][port] = dict()
            self.urls[scheme][hostname][port][path] = 0

    def find_status(self, verbose=False):
        for scheme in self.urls:
            if scheme in ['http', 'ftp']:
                connection = client.HTTPConnection
            elif scheme == 'https':
                connection = client.HTTPSConnection
            for hostname in self.urls[scheme]:
                for port in self.urls[scheme][hostname]:
                    try:
                        conn = connection(hostname, port)
                        for path in self.urls[scheme][hostname][port]:
                            u = scheme+'://'+hostname+':'+port+path
                            if verbose:
                                print('Checking', u)
                            conn.request("GET", path)
                            response = conn.getresponse()
                            self.urls[scheme][hostname][port][path] = response.status
                            conn.close()
                    except Exception as err:
                        for path in self.urls[scheme][hostname][port]:
                            self.urls[scheme][hostname][port][path] = err
                            print('Checking', u)
                            pass
    def print_status(self):
        for scheme in self.urls:
            for hostname in self.urls[scheme]:
                for port in self.urls[scheme][hostname]:
                    for path in self.urls[scheme][hostname][port]:
                        u = scheme+'://'+hostname+':'+port+path
                        print(u,':::', self.urls[scheme][hostname][port][path])
            
#

if len(sys.argv) != 2:
    print("Incorrect parameters passed")
    print("Usage:")
    print("\t"+sys.argv[0]+" <file>")
    print("\tWhere <file> is file from which URLs will be extracted")
    print("\n\n")
    sys.exit(1)

if not os.path.isfile(sys.argv[1]):
    print("File '"+sys.argv[1]+"' does not exists")
    sys.exit(1)

u = url()

try:
    for line in open(sys.argv[1]):
        u.parse_line(line)
except KeyboardInterrupt:
    print("Keyboard interrupt. Aborting")
    sys.exit(2)

    
u.find_status(verbose=True)
u.print_status()
