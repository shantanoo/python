#!/opt/local/bin/python
# encoding: utf-8


from BeautifulSoup import BeautifulSoup
from cgi import escape

import urllib2
import sys
import urlparse
import os
import codecs


param = sys.argv[1]

url = ''

try:
    if param == '-stdin':
        soup = BeautifulSoup(unicode(sys.stdin.read(), 'utf-8', errors='replace'))
    elif os.path.exists(param):
        soup = BeautifulSoup(codecs.open(param, 'r', encoding='utf-8'))
    else:
        url = param
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:6.0.2) Gecko/20100101 Firefox/6.0.2'}
        request = urllib2.Request(url, headers=headers)
        handle = urllib2.build_opener()
        soup = BeautifulSoup(unicode(handle.open(request).read(), 'utf-8', errors='replace'))
except:
    print 'Unable to read data from given parameters'
    sys.exit(1)
tags = soup('a')
for tag in tags:
    href = tag.get('href')
    if href:
        url = urlparse.urljoin(url, escape(href))
        print url
