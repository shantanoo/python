#!/usr/bin/env python3

import sys
import argparse


'''
Finds pi using following formula

pi = (4/1) - (4/3) + (4/5) - (4/7) ...
'''


def gen_num(i=100):
    x = 4 * 10 ** i
    while True:
        yield x


def op():
    while True:
        yield 1
        yield -1


parser = argparse.ArgumentParser()
parser.add_argument('-d', '--digits',
                    help='Number of digits of pi', type=int, default=20)
parser.add_argument('-s', '--seq',
                    help='Length of sequence', type=int, default=20)

args = parser.parse_args(sys.argv[1:])
num = gen_num(args.digits)
g = op()

print(sum(
    map(lambda x: (x[0] // x[1]) * next(g), zip(num, range(1, args.seq, 2))))
)
