import osmapi


'''
Sample code for updating OSM

'''

api = osmapi.OsmApi(passwordfile = '_change_me_')

w = api.WayGet('_change_me_integer_')

api.ChangesetCreate({u"comment": '_change_me'})

w['tag']['name:mr'] = '_change_me_'

q = api.WayUpdate(w)

api.ChangesetClose()
