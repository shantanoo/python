#!/usr/bin/env python3
# encoding: utf-8

import sys
import feedparser

LOGIN = None # _change_me_
APIKEY = None # _change_me_
api = None
try:
    import bitly
    if LOGIN and APIKEY:
        api = bitly.Api(login=LOGIN, apikey=APIKEY)
except:
    pass

feed = {}
feed['slashdotscience'] = 'http://rss.slashdot.org/Slashdot/slashdotScience'
feed['slashdotit'] = 'http://rss.slashdot.org/Slashdot/slashdotIT'
feed['googlenews'] = 'http://news.google.co.in/news?pz=1&cf=all&ned=in&hl=en&output=rss'

try:
    feeds = sys.argv[1].split(',')
except:
    feeds = list(feed.keys())

for f in feeds:
    print(f)
    try:
        d = feedparser.parse(feed[f])
    except:
        continue
    for x in d.entries:
        if api:
            try:
                url = api.shorten(x.link)
            except Exception as e:
                url = x.link
                print(e)
        else:
            url = x.link
        msg = '%s %s' % (x.title_detail.value, url)
        print(msg.encode('utf8'), len(msg.encode('utf8')))
