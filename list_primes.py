#!/usr/bin/env python
import sys
import math

def help():
    print('invalid parameters passed')
    print('Usage:')
    print(sys.argv[0] + ' <from number> <to number>')
    print(sys.argv[0] + ' <to number>')
    sys.exit(1)

if len(sys.argv) not in [2,3]:
    help()

try:
    no_from = int(sys.argv[1])
    if len(sys.argv) == 3:
        no_to = int(sys.argv[2])
    else:
        no_from = 1
        no_to = int(sys.argv[1])
except:
    help()

if no_from > no_to:
    print('No primes available in the given range')
    sys.exit(1)

if no_from < 2:
    no_from = 2

l = [x for x in range(no_from, no_to) if x not in [j for i in range(2, int(math.sqrt(float(no_to)))) for j in range(i*2, no_to, i)]]
print('There are '+str(len(l))+' primes between '+ str(no_from)+' and '+str(no_to))
print(l)
