#!/usr/local/Cellar/python3/3.3.0/bin/python3
# encoding: utf-8
import re
import sys
import urllib.request
import requests as reqs
import os

from bs4 import BeautifulSoup

'''
Sample code for downloading the TIFF scanned books from http://www.dli.ernet.in/
'''
url = 'http://202.41.82.144'
search = '/cgi-bin/advsearch_db.cgi'
data = {}
data['perPage'] = 25
data['listStart'] = 0
data['title1'] = ''
data['author1'] = ''
data['year1'] = ''
data['year2'] = ''
data['subject1'] = ''
data['subject1']= ''
data['language1'] = 'Marathi'
data['scentre'] = 'Any'
data['Search'] = 'Search'
r = reqs.post(url+search, data=data)
s = BeautifulSoup(r.text)
for a in s.findAll('a'):
    print(a.text.strip())
    tmp = dict(z.split('=',1) for z in re.sub('[\r\n\t]', '', a['href']).split('&') if '=' in z)
    if not tmp.get('url', None):
        continue
    book_path = a.text.strip()
    if os.path.exists(book_path):
        print('Skipping: %s' % book_path)
        continue
    os.makedirs(book_path)
    p = tmp['url']
    book = url+tmp['url'].strip()+'/PTIFF'
    print(book)
    r = reqs.get(book)
    t = BeautifulSoup(r.text)
    for b in t.findAll('a', href=re.compile('tif')):
        print(book+'/'+b['href'])
        urllib.request.urlretrieve(book+'/'+b['href'], filename=book_path+'/'+b['href'])
