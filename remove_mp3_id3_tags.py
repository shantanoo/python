#!/usr/bin/env python

# mutagen currently supports python 2.x only
# Install using pip:
#   pip install mutagen

from mutagen.mp3 import MP3
import sys

for fn in sys.argv[1:]:
    try:
        t = MP3(fn)
        t.delete()
        t.save()
        print('Removed ID3 tags from "%s"' % fn)
    except Exception as e:
        print('Unable to remove ID3 tags from "%s"' % fn)
        print('Error: %s' % e)

