#!/usr/bin/env python3

import re
import sys
import os


'''
Extracts email address from the file or STDIN
'''

if len(sys.argv) != 2:
    print("Incorrect parameters passed")
    print("Usage:")
    print("\t" + sys.argv[0] + " <file>")
    print("\tWhere <file> is file from which e-mail"
          " addresses will be extracted")
    print("\tOr use '-stdin' to use STDIN or pipe from other command")
    print("\n\n")
    sys.exit(1)

p = re.compile(r'([a-z]|\d|_|\.)+@([a-z]|\d)+(\.(([a-z]|\d)*))*')
if sys.argv[1] == '-stdin':
    f = sys.stdin
elif os.path.isfile(sys.argv[1]):
    f = open(sys.argv[1])
else:
    print("File '%s' does not exists" % sys.argv[1])
    sys.exit(1)

try:
    for line in f:
        iterator = p.finditer(line)
        for match in iterator:
            print(match.group())
except KeyboardInterrupt:
    print("Keyboard interrupt. Aborting")
    sys.exit(2)
