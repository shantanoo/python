#!/opt/local/bin/python

import sys

try:
    n = int(sys.argv[1])
except:
    print 'Usage: %s n' % sys.argv[0]
    print '       Where n is odd or 4'
    sys.exit()

a = [[False for x in range(n)] for y in range(n)]

def is_on_diagonal(n, r, c):
    print r, c, n, 
    t = n - r
    if r == c or c + t == n:
        print 1
        return True
    print 0
if n % 2:
    row = 0
    col = n/2
    a[row][col] = 1


    for x in xrange(2,(n**2)+1):
        nr = row - 1
        nc = col + 1
        if nr < 0:
            nr = n-1
        if nc > n-1:
            nc = 0
        if a[nr][nc]:
            nr = row + 1
            nc = col
            if row > n-1:
                row = 0
            if col < 0:
                col = n-1
        a[nr][nc] = x
        row, col = nr, nc

    for y in a:
        print y, sum(y)

elif not n % 4:
    if n != 4:
        print '%dx%d not yet supported' % (n,n)
        sys.exit()
    for x in xrange(n):
        a[x][x] = True
    for x in zip(range(n),range(n-1,-1, -1)):
        a[x[0]][x[1]] = True
    
    cnt = 1
    for x in xrange(n):
        for y in xrange(n):
            if a[x][y]:
                a[x][y] = cnt
            cnt += 1
    #
    cnt = 1
    for x in xrange(n-1,-1,-1):
        for y in xrange(n-1,-1,-1):
            if not a[x][y]:
                a[x][y] = cnt
            cnt += 1
    
    for x in a:
        print x, sum(x)

else:
    print 'Not supported'
