#!/opt/local/bin/python

'''
Updates the Adium and iChat status message from cli.
'''
import random
import sys
import commands
import codecs

msg = random.choice(list(codecs.open(sys.argv[1]))).strip()


cmd = """arch -i386 osascript -e 'set message to "%s"
    tell application "System Events"
    if exists process "iChat" then tell application "iChat" to set the status message to message
    if exists process "Adium" then tell application "Adium" to set status message of every account to message
    end tell'""" % msg

print commands.getoutput(cmd)
