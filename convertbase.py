#!/usr/bin/env python
import sys


def convert(number, ibase, obase):
    '''
    Converts number from ibase to obase.
    '''
    if ibase == obase:
        return str(number)
    newno = int(number, ibase)
    if obase == 10:
        return str(newno)
    data = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZa'
    ret = []
    while newno:
        ret.append(data[newno % obase])
        newno = newno // obase
    return ''.join(ret[::-1])


if len(sys.argv) != 4:
    print("Invalid number of parameters passed")
    print("Usage:")
    print(sys.argv[0] + " <number> <input_base> <output_base>")
    sys.exit(1)


print(convert(sys.argv[1], int(sys.argv[2]), int(sys.argv[3])))
